#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass memoir
\use_default_options true
\maintain_unincluded_children false
\language spanish
\language_package default
\inputencoding utf8
\fontencoding global
\font_roman palatino
\font_sans default
\font_typewriter default
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\pdf_title "Conociendo postgreSQL 9.1"
\pdf_author "Jesús Lara"
\pdf_keywords "postgres, postgresql"
\pdf_bookmarks true
\pdf_bookmarksnumbered false
\pdf_bookmarksopen false
\pdf_bookmarksopenlevel 1
\pdf_breaklinks false
\pdf_pdfborder false
\pdf_colorlinks false
\pdf_backref false
\pdf_pdfusetitle true
\papersize default
\use_geometry false
\use_amsmath 1
\use_esint 1
\use_mhchem 1
\use_mathdots 1
\cite_engine basic
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Part
Virtualización en GNU/Linux
\end_layout

\begin_layout Chapter
Introducción
\end_layout

\begin_layout Standard
Virtualización es la creación -a través de software- de una versión virtual
 de algún recurso tecnológico, como puede ser una plataforma de hardware,
 un sistema operativo, un dispositivo de almacenamiento u otros recursos
 de red.
 Dicho de otra manera, se refiere a la abstracción de los recursos de una
 computadora base, llamada Hypervisor o VMM (Virtual Machine Monitor) que
 crea una capa de abstracción entre el hardware de la máquina física (llamada
 host) y el sistema operativo de la máquina virtual (virtual machine, guest),
 dividiéndose el recurso en uno o más entornos de ejecución.
 Esta capa de software (VMM) maneja, gestiona y arbitra los cuatro recursos
 principales de una computadora (CPU, Memoria, Almacenamiento y Conexiones
 de Red) y así podrá repartir dinámicamente dichos recursos entre todas
 las máquinas virtuales definidas en el computador base o host.
 Esto hace que se puedan tener varios ordenadores virtuales ejecutándose
 en el mismo ordenador físico.
\end_layout

\begin_layout Standard
En Linux puede contar con al menos 3 posibilidades de virtualización para
 entornos de empresa, cada una con una utilidad específica:
\end_layout

\begin_layout Itemize
LXC: Permite la creación de contenedores (jaula ligera conteniendo una versión
 del sistema operativo Linux)
\end_layout

\begin_layout Itemize
Xen: Utiliza la tecnología de Hypervisor para crear una virtualización absoluta
 de contenedores Linux.
\end_layout

\begin_layout Itemize
KVM: Utiliza las características de nuevos CPUs para virtualizar cualquier
 sistema operativo sobre GNU/Linux
\end_layout

\begin_layout Section
LXC
\end_layout

\begin_layout Subsection
Introducción
\end_layout

\begin_layout Standard
Tecnología de "contenedores", más que una "virtualización" es una "contextualiza
ción", aislamiento en un contexto específico (CHROOT) de una versión de
 GNU/Linux imbuida (VPS) en el interior de otra.
 (HOST)
\end_layout

\begin_layout Standard
Dentro de los contenedores reside otra versión de GNU/Linux, la cual posee
 su propia interfaz de red, se pueden aplicar cuotas de disco/CPU/RAM y
 se pueden detener, apagar y/o suspender.
\end_layout

\begin_layout Standard
Es un "chroot" mejorado puesto que las facilidades de administración y ejecución
 basadas en chroot (lxc-execute) pueden ser utilizadas para LXC, también
 las tecnologías de creación de espacios aislados (isolated) como debootstrap
 (Debian|Ubuntu), rpmstrap (Fedora,CentOS) pero con las capacidades de virtualiz
ación que incopora LXC.
\end_layout

\begin_layout Standard
LXC es nativa en el Kernel Linux a partir de la versión 2.6.32 y utiliza caracterí
sticas nativas del Kernel Linux como los CGROUPS y los Namespaces.
\end_layout

\begin_layout Standard
También está soportada por Libvirt, de tal manera que cualquier plataforma
 de gestión que utilice libvirt, sera capaz de administrar VPS construidos
 con LXC.
\end_layout

\begin_layout Subsection
Instalación
\end_layout

\begin_layout Standard
\begin_inset listings
lstparams "style=consola"
inline false
status open

\begin_layout Plain Layout

# aptitude install lxc
\end_layout

\end_inset


\end_layout

\begin_layout Subsection
Configuración
\end_layout

\begin_layout Standard
Crear el directorio de los contenedores y configuraciones:
\end_layout

\begin_layout Standard
\begin_inset listings
lstparams "style=consola"
inline false
status open

\begin_layout Plain Layout

mkdir /srv/lxc
\end_layout

\end_inset


\end_layout

\begin_layout Standard
Configurar 
\emph on
/etc/default/lxc
\emph default
 para que inicie al arranque y utilice el directorio de configuraciones:
\end_layout

\begin_layout Standard
\begin_inset listings
lstparams "style=archivo"
inline false
status open

\begin_layout Plain Layout

# Comment out to run the lxc init script
\end_layout

\begin_layout Plain Layout

RUN=yes
\end_layout

\begin_layout Plain Layout

# Directory containing the container configurations
\end_layout

\begin_layout Plain Layout

CONF_DIR=/srv/lxc
\end_layout

\end_inset


\end_layout

\begin_layout Standard
Verificar que tenemos soporte para CGROUPS en el kernel
\end_layout

\begin_layout Standard
\begin_inset listings
lstparams "style=consola"
inline false
status open

\begin_layout Plain Layout

cat /proc/filesystems | grep cgroup
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset listings
lstparams "style=archivo"
inline false
status open

\begin_layout Plain Layout

nodev cgroup
\end_layout

\end_inset


\end_layout

\begin_layout Standard
Y verificamos la carga de los cgroups:
\end_layout

\begin_layout Standard
\begin_inset listings
lstparams "style=consola"
inline false
status open

\begin_layout Plain Layout

cat /proc/cgroups 
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset listings
lstparams "style=archivo"
inline false
status open

\begin_layout Plain Layout

#subsys_name hierarchy num_cgroups enabled
\end_layout

\begin_layout Plain Layout

cpuset 0 1 1
\end_layout

\begin_layout Plain Layout

ns 0 1 1
\end_layout

\begin_layout Plain Layout

cpu 0 1 1
\end_layout

\begin_layout Plain Layout

cpuacct 0 1 1
\end_layout

\begin_layout Plain Layout

devices 0 1 1
\end_layout

\begin_layout Plain Layout

freezer 0 1 1
\end_layout

\begin_layout Plain Layout

net_cls 0 1 1
\end_layout

\end_inset


\end_layout

\begin_layout Standard
Montar CGROUPS en el directorio 
\emph on
/sys/fs/cgroup/
\emph default
:
\end_layout

\begin_layout Standard
\begin_inset listings
lstparams "style=consola"
inline false
status open

\begin_layout Plain Layout

mount -t cgroup none /sys/fs/cgroup
\end_layout

\begin_layout Plain Layout

chmod 0755 /sys/fs/cgroup
\end_layout

\end_inset


\end_layout

\begin_layout Standard
Y verificamos:
\end_layout

\begin_layout Standard
\begin_inset listings
lstparams "style=archivo"
inline false
status open

\begin_layout Plain Layout

/sys/fs/cgroup/
\end_layout

\begin_layout Plain Layout

    cgroup.procs
\end_layout

\begin_layout Plain Layout

    cpuacct.stat
\end_layout

\begin_layout Plain Layout

    cpuacct.usage
\end_layout

\begin_layout Plain Layout

    cpuacct.usage_percpu
\end_layout

\begin_layout Plain Layout

    cpuset.cpu_exclusive
\end_layout

\begin_layout Plain Layout

    cpuset.cpus
\end_layout

\begin_layout Plain Layout

    cpuset.mem_exclusive
\end_layout

\begin_layout Plain Layout

    cpuset.mem_hardwall
\end_layout

\begin_layout Plain Layout

    cpuset.memory_migrate
\end_layout

\begin_layout Plain Layout

    cpuset.memory_pressure
\end_layout

\begin_layout Plain Layout

    cpuset.memory_pressure_enabled
\end_layout

\begin_layout Plain Layout

    cpuset.memory_spread_page
\end_layout

\begin_layout Plain Layout

    cpuset.memory_spread_slab
\end_layout

\begin_layout Plain Layout

    cpuset.mems
\end_layout

\begin_layout Plain Layout

    cpuset.sched_load_balance
\end_layout

\begin_layout Plain Layout

    cpuset.sched_relax_domain_level
\end_layout

\begin_layout Plain Layout

    cpu.shares
\end_layout

\begin_layout Plain Layout

    devices.allow
\end_layout

\begin_layout Plain Layout

    devices.deny
\end_layout

\begin_layout Plain Layout

    devices.list
\end_layout

\begin_layout Plain Layout

    net_cls.classid
\end_layout

\begin_layout Plain Layout

    notify_on_release
\end_layout

\begin_layout Plain Layout

    release_agent
\end_layout

\begin_layout Plain Layout

    tasks
\end_layout

\end_inset


\end_layout

\begin_layout Standard
Posteriormente, fijarlo en el 
\emph on
/etc/fstab
\end_layout

\begin_layout Standard
\begin_inset listings
lstparams "style=archivo"
inline false
status open

\begin_layout Plain Layout

none /sys/fs/cgroup cgroup cpuset,cpu,memory,cpuacct,devices,freezer,net_cls
 0 0
\end_layout

\end_inset


\end_layout

\begin_layout Standard
Y en las udev, para que pueda ser creado durante el proceso de arranque:
\end_layout

\begin_layout Standard
\begin_inset listings
inline false
status open

\begin_layout Plain Layout

mkdir /lib/udev/devices/cgroup
\end_layout

\end_inset


\end_layout

\begin_layout Standard
* Montar las librerías de gestión de cgroups
\end_layout

\begin_layout Standard
\begin_inset listings
inline false
status open

\begin_layout Plain Layout

aptitude install libcgroup1
\end_layout

\end_inset


\end_layout

\begin_layout Standard
- Y reiniciamos el equipo.
\end_layout

\begin_layout Standard
- luego, podemos verificar con lxc-checkconfig para determinar si está bien
 soportado CGROUPS y LXC
\end_layout

\begin_layout Standard
\begin_inset listings
lstparams "style=consola"
inline false
status open

\begin_layout Plain Layout

# lxc-checkconfig 
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset listings
lstparams "style=archivo"
inline false
status open

\begin_layout Plain Layout

Kernel config /proc/config.gz not found, looking in other places...
\end_layout

\begin_layout Plain Layout

Found kernel config file /boot/config-2.6.32-5-xen-amd64
\end_layout

\begin_layout Plain Layout

--- Namespaces ---
\end_layout

\begin_layout Plain Layout

Namespaces: enabled
\end_layout

\begin_layout Plain Layout

Utsname namespace: enabled
\end_layout

\begin_layout Plain Layout

Ipc namespace: enabled
\end_layout

\begin_layout Plain Layout

Pid namespace: enabled
\end_layout

\begin_layout Plain Layout

User namespace: enabled
\end_layout

\begin_layout Plain Layout

Network namespace: enabled
\end_layout

\begin_layout Plain Layout

Multiple /dev/pts instances: enabled
\end_layout

\begin_layout Plain Layout

--- Control groups ---
\end_layout

\begin_layout Plain Layout

Cgroup: enabled
\end_layout

\begin_layout Plain Layout

Cgroup namespace: enabled
\end_layout

\begin_layout Plain Layout

Cgroup device: enabled
\end_layout

\begin_layout Plain Layout

Cgroup sched: enabled
\end_layout

\begin_layout Plain Layout

Cgroup cpu account: enabled
\end_layout

\begin_layout Plain Layout

Cgroup memory controller: missing
\end_layout

\begin_layout Plain Layout

Cgroup cpuset: enabled
\end_layout

\begin_layout Plain Layout

--- Misc ---
\end_layout

\begin_layout Plain Layout

Veth pair device: enabled
\end_layout

\begin_layout Plain Layout

Macvlan: enabled
\end_layout

\begin_layout Plain Layout

Vlan: enabled
\end_layout

\begin_layout Plain Layout

File capabilities: enabled
\end_layout

\end_inset


\end_layout

\begin_layout Chapter
Xen
\end_layout

\begin_layout Section
Introducción
\end_layout

\begin_layout Standard
Motor de máquinas virtuales, de código abierto, diseñado para dar virtualización
 con aislamiento seguro y todas las características de un sistema operativo.
\end_layout

\begin_layout Standard
Xen utiliza para-virtualización, es este caso, tanto el host como el cliente
 sufren "modificaciones" a nivel de su kernel para poder ejecutarse e interactua
r con un mínimo de penalty en el rendimiento.
\end_layout

\begin_layout Standard
Mediante técnicas de paravirtualización, un host puede ejecutar núcleos
 modificados de Linux, NetBSD y FreeBSD.
\end_layout

\begin_layout Standard
Virtualización Completa: Se incorpora a Xen la posibilidad de utilizar las
 tecnologías de virtualización asistida por Hardware (CPU) de Intel-VT y
 AMD-V, con lo cual se permite ejecutar clientes "no-modificados" sobre
 la máquina host, con esta técnica se puede ejecutar cualquier sistema operativo.
\end_layout

\begin_layout Standard
A partir del kernel 2.6.32, todo kernel Linux puede ser ejecutado "paravirtualizad
o" sobre un hypervisor, puesto que los "parches" de soporte para paravirtualizac
ión han sido incorporados al upstream del kernel.
\end_layout

\begin_layout Section
Instalación de Xen
\end_layout

\begin_layout Standard
Paquetes requeridos:
\end_layout

\begin_layout Standard
\begin_inset listings
lstparams "style=consola"
inline false
status open

\begin_layout Plain Layout

# apt-get install xen-tools xen-utils xen-utils-common xenstore-utils xen-hyperv
isor
\end_layout

\end_inset


\end_layout

\begin_layout Standard
Debemos reiniciar con el nuevo kernel Xen, para ello, modificamos GRUB para
 iniciar por defecto por Xen:
\end_layout

\begin_layout Standard
\begin_inset listings
lstparams "style=consola"
inline false
status open

\begin_layout Plain Layout

sed -i 's/GRUB_DEFAULT=.*
\backslash
+/GRUB_DEFAULT="Xen 4.1-amd64"/' /etc/default/grub
\end_layout

\begin_layout Plain Layout

update-grub
\end_layout

\end_inset


\end_layout

\begin_layout Standard
Definimos el Toolstack a 
\begin_inset Quotes eld
\end_inset

xm
\begin_inset Quotes erd
\end_inset

:
\end_layout

\begin_layout Standard
\begin_inset listings
lstparams "style=consola"
inline false
status open

\begin_layout Plain Layout

sudo sed -i 's/TOOLSTACK=.*
\backslash
+/TOOLSTACK="xm"/' /etc/default/xen
\end_layout

\end_inset


\end_layout

\begin_layout Standard
Y reiniciamos el equipo:
\end_layout

\begin_layout Standard
\begin_inset listings
lstparams "style=consola"
inline false
status open

\begin_layout Plain Layout

# reboot
\end_layout

\end_inset


\end_layout

\begin_layout Standard
Al iniciar, verificamos que estamos en el nuevo kernel:
\end_layout

\begin_layout Standard
\begin_inset listings
inline false
status open

\begin_layout Plain Layout

uname -r
\end_layout

\begin_layout Plain Layout

2.6.32-5-xen-amd64
\end_layout

\end_inset


\end_layout

\begin_layout Standard
Y las capacidades que soporta Xen:
\end_layout

\begin_layout Standard
\begin_inset listings
lstparams "style=consola"
inline false
status open

\begin_layout Plain Layout

# xm info
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset listings
lstparams "style=archivo"
inline false
status open

\begin_layout Plain Layout

host : lab.test.com
\end_layout

\begin_layout Plain Layout

release : 2.6.32-5-xen-amd64
\end_layout

\begin_layout Plain Layout

version : #1 SMP Wed Jan 12 05:46:49 UTC 2011
\end_layout

\begin_layout Plain Layout

machine : x86_64
\end_layout

\begin_layout Plain Layout

nr_cpus : 2
\end_layout

\begin_layout Plain Layout

nr_nodes : 1
\end_layout

\begin_layout Plain Layout

cores_per_socket : 2
\end_layout

\begin_layout Plain Layout

threads_per_core : 1
\end_layout

\begin_layout Plain Layout

cpu_mhz : 1596
\end_layout

\begin_layout Plain Layout

hw_caps : bfebfbff:20100800:00000000:00000940:0000e3bd:00000000:00000001:0000000
0
\end_layout

\begin_layout Plain Layout

virt_caps : hvm
\end_layout

\begin_layout Plain Layout

total_memory : 4022
\end_layout

\begin_layout Plain Layout

free_memory : 900
\end_layout

\begin_layout Plain Layout

node_to_cpu : node0:0-1
\end_layout

\begin_layout Plain Layout

node_to_memory : node0:900
\end_layout

\begin_layout Plain Layout

node_to_dma32_mem : node0:834
\end_layout

\begin_layout Plain Layout

max_node_id : 0
\end_layout

\begin_layout Plain Layout

xen_major : 4
\end_layout

\begin_layout Plain Layout

xen_minor : 0
\end_layout

\begin_layout Plain Layout

xen_extra : .1
\end_layout

\begin_layout Plain Layout

xen_caps : xen-3.0-x86_64 xen-3.0-x86_32p hvm-3.0-x86_32 hvm-3.0-x86_32p hvm-3.0-x86_
64 
\end_layout

\begin_layout Plain Layout

xen_scheduler : credit
\end_layout

\begin_layout Plain Layout

xen_pagesize : 4096
\end_layout

\begin_layout Plain Layout

platform_params : virt_start=0xffff800000000000
\end_layout

\begin_layout Plain Layout

xen_changeset : unavailable
\end_layout

\begin_layout Plain Layout

xen_commandline : placeholder
\end_layout

\begin_layout Plain Layout

cc_compiler : gcc version 4.4.5 (Debian 4.4.5-10) 
\end_layout

\begin_layout Plain Layout

cc_compile_by : waldi
\end_layout

\begin_layout Plain Layout

cc_compile_domain : debian.org
\end_layout

\begin_layout Plain Layout

cc_compile_date : Wed Jan 12 14:04:06 UTC 2011
\end_layout

\begin_layout Plain Layout

xend_config_format : 4
\end_layout

\end_inset


\end_layout

\begin_layout Standard
entre las que se cuenta, virt-caps: HVM (soporte a virtualización por Hardware
 para arquitecturas x86 y x86_64)
\end_layout

\begin_layout Standard
Editamos el archivo 
\emph on
/etc/xen/xend-config.sxp
\emph default
 y habilitamos el demonio XML-RPC (que utilizar libvirt, virt-manager y
 otras herramientas para gestionar Xen).
\end_layout

\begin_layout Standard
\begin_inset listings
lstparams "style=archivo"
inline false
status open

\begin_layout Plain Layout

(logfile /var/log/xen/xend.log)
\end_layout

\begin_layout Plain Layout

(xend-unix-server yes)
\end_layout

\begin_layout Plain Layout

(xend-tcp-xmlrpc-server yes)
\end_layout

\begin_layout Plain Layout

(xend-unix-xmlrpc-server yes)
\end_layout

\begin_layout Plain Layout

(xend-unix-path /var/lib/xend/xend-socket)
\end_layout

\begin_layout Plain Layout

(xend-tcp-xmlrpc-server-address 'localhost') # use 0.0.0.0 para escuchar por
 todas las IP
\end_layout

\begin_layout Plain Layout

(xend-tcp-xmlrpc-server-port 8006)
\end_layout

\begin_layout Plain Layout

(xend-port 8000)
\end_layout

\end_inset


\end_layout

\begin_layout Standard
* Si deseamos que Xen gestione el bridge, descomentamos
\end_layout

\begin_layout Standard
\begin_inset listings
inline false
status open

\begin_layout Plain Layout

(network-script network-bridge)
\end_layout

\end_inset


\end_layout

\begin_layout Standard
O en su defecto, configuramos un bridge para la red como lo indicamos en
 el Capítulo de redes.
\end_layout

\begin_layout Standard
* Reiniciamos el demonio xend
\end_layout

\begin_layout Standard
\begin_inset listings
inline false
status open

\begin_layout Plain Layout

/etc/init.d/xend restart
\end_layout

\end_inset


\end_layout

\begin_layout Standard
* Verificamos el demonio TCP:
\end_layout

\begin_layout Standard
\begin_inset listings
lstparams "style=consola"
inline false
status open

\begin_layout Plain Layout

# lsof -i tcp
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset listings
lstparams "style=archivo"
inline false
status open

\begin_layout Plain Layout

COMMAND PID USER FD TYPE DEVICE SIZE/OFF NODE NAME
\end_layout

\begin_layout Plain Layout

xend 1462 root 32u IPv4 7423 0t0 TCP localhost:8006 (LISTEN)
\end_layout

\end_inset


\end_layout

\begin_layout Standard
Y el socket unix:
\end_layout

\begin_layout Standard
file /var/lib/xend/xend-socket
\end_layout

\begin_layout Standard
/var/lib/xend/xend-socket: socket
\end_layout

\begin_layout Standard
Con estas modificaciones, ya podemos configurar xen-tools para la creación
 de máquinas virtuales.
\end_layout

\begin_layout Subsection
Creación de máquinas con Xen-tools
\end_layout

\begin_layout Standard
Para crear máquinas virtuales con Xen, se utiliza una utilidad de xen-tools
 llamada xen-create-image que nos permite crear cualquier tipo de sistema
 GNU/Linux basado en Debian (Debian, Ubuntu, etc).
\end_layout

\begin_layout Standard
La creación de máquinas virtuales se realiza con el comando:
\end_layout

\begin_layout Standard
\begin_inset listings
lstparams "style=consola"
inline false
status open

\begin_layout Plain Layout

# xen-create-image --hostname=maquina1 --dhcp --dist=squeeze --memory=256M
 --size=10G --lvm=vgvm
\end_layout

\end_inset


\end_layout

\begin_layout Standard
Donde:
\end_layout

\begin_layout Itemize
hostname: define el nombre de la máquina
\end_layout

\begin_layout Itemize
dhcp: define que la máquina recibirá nombre por DHCP
\begin_inset Foot
status collapsed

\begin_layout Plain Layout
Si se desea crear una máquina con configuración de red manual, se deben
 pasar los parámetros --ip --gateway y --netmask
\end_layout

\end_inset


\end_layout

\begin_layout Standard
La configuración de una máquina (archivo .cfg) tiene la siguiente sintáxis
 (ejemplo):
\end_layout

\begin_layout Standard
archivo: /etc/xen/ubuntu.cfg
\end_layout

\begin_layout Standard
\begin_inset listings
lstparams "style=archivo"
inline false
status open

\begin_layout Plain Layout

name = "ubuntu"
\end_layout

\begin_layout Plain Layout

memory = 256
\end_layout

\begin_layout Plain Layout

disk = ['phy:/dev/<VG>/ubuntu,xvda,w']
\end_layout

\begin_layout Plain Layout

vif = [' ']
\end_layout

\begin_layout Plain Layout

kernel = "/var/lib/xen/images/ubuntu-netboot/vmlinuz"
\end_layout

\begin_layout Plain Layout

ramdisk = "/var/lib/xen/images/ubuntu-netboot/initrd.gz"
\end_layout

\begin_layout Plain Layout

extra = "debian-installer/exit/always_halt=true -- console=hvc0"
\end_layout

\end_inset


\end_layout

\begin_layout Standard

\end_layout

\begin_layout Section
Gestión básica de Xen
\end_layout

\begin_layout Standard
La gestión básica de Xen se realiza a través del comando xm (
\begin_inset Quotes eld
\end_inset

xen-manager
\begin_inset Quotes erd
\end_inset

) que nos permite encender, apagar y reiniciar máquinas virtuales, entre
 otras opciones:
\end_layout

\begin_layout Standard
Crear una instancia de máquina virtual creada con xen-tools
\end_layout

\begin_layout Standard
\begin_inset listings
inline false
status open

\begin_layout Plain Layout

# xm create maquina.cfg
\end_layout

\end_inset


\end_layout

\begin_layout Standard
Agregar una máquina virtual creada por xen-tools al entorno administrado
 de Xen
\end_layout

\begin_layout Standard
\begin_inset listings
inline false
status open

\begin_layout Plain Layout

# xm new -f maquina.cfg
\end_layout

\end_inset


\end_layout

\begin_layout Standard
Para iniciar una máquina administrada
\end_layout

\begin_layout Standard
\begin_inset listings
inline false
status open

\begin_layout Plain Layout

# xm start <maquina>
\end_layout

\end_inset


\end_layout

\begin_layout Standard
Y para apagarla:
\end_layout

\begin_layout Standard
\begin_inset listings
inline false
status open

\begin_layout Plain Layout

# xm shutdown <maquina>
\end_layout

\end_inset


\end_layout

\begin_layout Standard
Para eliminar una máquina del entorno administrado:
\end_layout

\begin_layout Standard
\begin_inset listings
inline false
status open

\begin_layout Plain Layout

# xm delete <maquina>
\end_layout

\end_inset


\end_layout

\begin_layout Standard
Para listar las máquinas iniciadas:
\end_layout

\begin_layout Standard
\begin_inset listings
lstparams "style=consola"
inline false
status open

\begin_layout Plain Layout

# xm list 
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset listings
inline false
status open

\begin_layout Plain Layout

Name ID Mem VCPUs State Time(s) Domain-0 0 945 1 r----- 11.3
\end_layout

\end_inset


\end_layout

\begin_layout Standard
Para reiniciar una máquina:
\end_layout

\begin_layout Standard
\begin_inset listings
lstparams "style=consola"
inline false
status open

\begin_layout Plain Layout

# xm reboot <maquina>
\end_layout

\end_inset


\end_layout

\end_body
\end_document
